module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/strongly-recommended',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    "vue/html-self-closing": ["error", {
      "html": {
        "void": "never",
        "normal": "any",
        "component": "any"
      },
      "svg": "always",
      "math": "always"
    }],
    "vue/component-name-in-template-casing": "off",
    "vue/no-parsing-error":"off",
    "standard/no-callback-literal":"off"
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  globals: {
    _: true,
    Velocity: true
  }
}
