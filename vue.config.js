const path = require('path')
// const webpack = require('webpack')

function resolve (dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  css: {
    // 注入全局样式变量
    loaderOptions: {
      sass: {
        data: `
          @import "@/scss/_variables.scss";
        `
      }
    }
  },
  devServer: {
    // 使本地服务可在局域网访问
    host: '0.0.0.0',
    disableHostCheck: true,
    // 跨域代理
    proxy: {
      '/api': {
        target: 'https://pwxdev.cleartv.cn/', // 对应自己的接口
        changeOrigin: true,
        ws: true,
        pathRewrite: {
          '^/api': ''
        }
      }
    }
  },
  // vscode内设置断点调试，https://cn.vuejs.org/v2/cookbook/debugging-in-vscode.html
  configureWebpack: {
    devtool: 'source-map'
  },
  chainWebpack: (config) => {
    // 打包时忽略配置文件
    config.plugin('copy').tap((args) => [[
      {
        from: './public',
        to: './',
        toType: 'dir',
        ignore: [
          'config.js'
        ]
      }
    ]])

    // 路径别名
    config.resolve.alias
      .set('@$', resolve('src'))
      .set('assets', resolve('src/assets'))
      .set('imgs', resolve('src/assets/imgs'))
      .set('components', resolve('src/components'))
      .set('static', resolve('src/static'))
  }
}
