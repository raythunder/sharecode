import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    currentTheme: localStorage.currentTheme || 'red'
  },
  mutations: {
    SET_THEME (state, theme) {
      state.currentTheme = theme
      localStorage.currentTheme = theme
    }
  },
  actions: {

  }
})
