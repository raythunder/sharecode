import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'theme',
      component: () => import('./views/theme')
    },
    {
      path: '/animation',
      name: 'animation',
      component: () => import('./views/animation')
    },
    {
      path: '/eventBus',
      name: 'eventBus',
      component: () => import('./views/eventBus')
    },
    {
      path: '/globalTimer',
      name: 'globalTimer',
      component: () => import('./views/globalTimer')
    },
    {
      path: '/globalWatch',
      name: 'globalWatch',
      component: () => import('./views/globalWatch')
    }
  ]
})
