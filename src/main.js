import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/scss/index.scss'

import EventBus from '@/utils/eventBus.js'
import GlobalTimer from '@/utils/globalTimer.js'
import CallOnDestroy from '@/utils/callOnDestroy.js'

Vue.use(EventBus)
Vue.use(GlobalTimer)
Vue.use(CallOnDestroy)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
