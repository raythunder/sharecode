class GlobalTimer {
  constructor (vue) {
    // 如果已存在定时器，则清除
    this.timer && clearInterval(this.timer)

    // 用作组件与定时器方法做映射
    if (!this.handles) {
      Object.defineProperty(this, 'handles', {
        value: [],
        enumerable: false
      })
    }

    // 初始化定时器
    this.timer = setInterval(() => {
      this.handles.forEach(obj => {
        obj.funs.forEach(fun => {
          fun()
        })
      })
    }, 1000)
  }
  $add (fun, vm) {
    // 查找映射表内组件id
    let target = this.handles.find(i => {
      return i.id === vm._uid
    })
    // 未找到则新建一个
    if (!target) {
      target = {
        id: vm._uid,
        funs: []
      }
      this.handles.push(target)
    }

    // 查找是否有重复的方法名
    let targetFun = target.funs.find(i => {
      return i.name === fun.name
    })
    // 未找到则存入对应组件id对应的数组
    !targetFun && target.funs.push(fun)
  }
  $remove (fun, vm) {
    let target = this.handles.find(i => {
      return i.id === vm._uid
    })
    if (target) {
      target.funs = target.funs.filter(item => {
        return item.name !== fun.name
      })
    }
  }
  // 移除组件内所有的定时器方法
  $removeAll (uid) {
    let targetIndex = this.handles.findIndex(i => {
      return i.id === uid
    })
    this.handles.splice(targetIndex, 1)
  }
}
// 写成Vue插件形式，直接引入然后Vue.use($GlobalTimer)进行使用
let $GlobalTimer = {}

$GlobalTimer.install = (Vue, option) => {
  Vue.prototype.$globalTimer = new GlobalTimer(Vue)
  Vue.mixin({
    beforeDestroy () {
      // 拦截beforeDestroy钩子自动销毁所有当前组件内的定时器方法
      this.$globalTimer.$removeAll(this._uid)
    }
  })
}

export default $GlobalTimer
