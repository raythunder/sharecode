# shareCode

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

#[「Vue实践」武装你的前端项目](https://juejin.im/post/5cab64ce5188251b19486041)

# [资源管理器示例](https://github.com/vue-cloudfront/vue-cloudfront)

# 配置文件添加时间戳，强制刷新
``` js
<script>document.write("<script type='text/javascript' src='./config.js?v=" + Date.now() + "'><\/script>");</script>
```


